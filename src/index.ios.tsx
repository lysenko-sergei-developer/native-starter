import './lib/index.ios';
import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('typenative', () => App);
