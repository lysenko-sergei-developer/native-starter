import './lib/index.android';
import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('typenative', () => App);
